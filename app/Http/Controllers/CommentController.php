<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::all();
        return view('comment.index', ['comments' => $comments]);
    }
//
//    public function create()
//    {
//
//
//    }
}
