<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::where('user_id',Auth::id())->get();
        return view('post.index', compact('posts'));

    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'nullable|image|max:2000'
        ]);

        $post = new Post();

        $post->title = $request->input('title');
        $post->description = $request->input('description');
        if($request->hasFile('image')){
            $fileName = str_random(30) . "." . $request->image->guessExtension();
            Storage::disk('public')->putFileAs(null,$request->file('image'), $fileName);
            $post->image = $fileName;
        }
        if($request->has('publish')){
            $post->publish = $request->input('publish');
        }
        else{
            $post->publish = 0;
        }
        $post->user_id = Auth::id();

        $post->save();

        return redirect('/post');
    }

    public
    function show($id)
    {
        $post = Post::find($id);
        return view('post.show', compact('post'));
    }

    public
    function edit($id)
    {
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    public
    function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'nullable|image|max:2000'
        ]);

        $post = Post::where('user_id', Auth::id())->find($id);
        $post->title = $request->title;
        $post->description = $request->description;
        if($request->hasFile('image')){
            $fileName = str_random(30) . "." . $request->image->guessExtension();
            Storage::disk('public')->putFileAs(null,$request->file('image'), $fileName);
            $post->image = $fileName;
        }
        if($request->input('publish') == 1){
            $post->publish = $request->input('publish');
        }
        else{
            $post->publish = 0;
        }
        Storage::delete($request->image);
        $post->save();

        return redirect('/post');
    }

    public
    function delete($id)
    {
        $post = Post::where('user_id', Auth::id())->find($id)->get();
        $file_name = $post->image;
        Post::destroy($id);
        Storage::delete($file_name);
        return redirect('/post');
    }

    public
    function publish($id)
    {
        $post = Post::where('user_id', Auth::id())->find($id);
        $post->publish = 1;

        $post->save();
        return redirect('/post');
    }

    public
    function unpublish($id)
    {
        $post = Post::where('user_id', Auth::id())->find($id);
        $post->publish = 0;

        $post->save();
        return redirect('/post');
    }
}
