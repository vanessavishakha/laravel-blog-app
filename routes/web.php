<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/post', 'PostController@index')->name('post.index');

    Route::get('/post/create', function () {
        return view('post/create');
    })->name('post.create');
    Route::post('/post/create', 'PostController@create')->name('post.create');

    Route::get('/post/{id}', 'PostController@show')->name('post.show');

    Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');
    Route::post('/post/edit/{id}', 'PostController@update')->name('post.edit');

    Route::get('/post/delete/{id}', 'PostController@delete')->name('post.delete');

    Route::get('/post/publish/{id}', 'PostController@publish')->name('post.publish');
    Route::get('/post/unpublish/{id}', 'PostController@unpublish')->name('post.unpublish');

    Route::get('/comment', 'CommentController@index')->name('comment.index');

});

