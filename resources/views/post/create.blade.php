@extends('layout')

@section('content')

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-8">

                <h2><p class="m-0 text-center text-white">ADD POST</p></h2>

                <form method="POST" action="{{ route('post.create') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                        @if ($errors->has('title'))
                            <span class="help-block alert alert-danger">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description"
                                  placeholder="Description"></textarea>
                        @if ($errors->has('description'))
                            <span class="help-block alert alert-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="image">Upload an image</label>
                        <input type="file" id="image" name="image">
                    </div>
                    <div class="form-group">
                        <label for="publish">Publish</label>
                        <input type="checkbox" id="publish" name="publish" value="1">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Post</button>
                    </div>
                </form>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

@endsection
