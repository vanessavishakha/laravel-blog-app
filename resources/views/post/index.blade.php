@extends('layout')

@section('content')


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="text-right">
               <a href="{{ route('post.create') }}" class="btn btn-primary">Add post</a>
            </div>

            <!-- Post Content Column -->
            <div class="col-lg-8">

                <h2 style="text-align: center">POSTS</h2>

            @foreach($posts as $post)
                <!-- Title -->
                    <a href="{{ route('post.show',$post->id) }}">
                        <h1 class="mt-4">{{ $post->title }}</h1>
                    </a>

                    <hr>

                    <p>Posted on {{ $post->created_at}}</p>

                    <hr>

                    <!-- Preview Image -->
                    @if($post->image != NULL)
                        <img style="width: 200px; height: 200px" class="img-fluid rounded" src="{{ \Storage::url($post->image) }}" alt="">
                    @endif

                    <hr>

                    <!-- Post Content -->
                    @if(strlen($post->description) > 500)
                        <p class="lead">{{ substr($post->description,0,500) }}[...]<a href="{{ route('post.show',$post->id) }}" class="btn btn-primary">Read More</a></p>
                    @else
                        <p class="lead">{{ $post->description }}</p>
                    @endif

                    <hr>

                    <a href="{{ route('post.show',$post->id) }}">View</a>
                    <a href="{{ route('post.edit',$post->id) }}">Edit</a>
                    <a href="{{ route('post.delete',$post->id) }}">Delete</a>
                    @if($post->publish == 1)
                        <a href="{{ route('post.publish',$post->id) }}">Publish</a>
                    @else
                        <a href="{{ route('post.unpublish',$post->id) }}">Unpublish</a>
                    @endif
                @endforeach
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

@endsection

{{--<!-- Comments Form -->--}}
{{--<div class="card my-4">--}}
{{--<h5 class="card-header">Leave a Comment:</h5>--}}
{{--<div class="card-body">--}}
{{--<form>--}}
{{--<div class="form-group">--}}
{{--<textarea class="form-control" rows="3"></textarea>--}}
{{--</div>--}}
{{--<button type="submit" class="btn btn-primary">Submit</button>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}

{{--<!-- Single Comment -->--}}
{{--<div class="media mb-4">--}}
{{--<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">--}}
{{--<div class="media-body">--}}
{{--<h5 class="mt-0">Commenter Name</h5>--}}
{{--Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.--}}
{{--Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc--}}
{{--ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.--}}
{{--</div>--}}
{{--</div>--}}

{{--<!-- Comment with nested comments -->--}}
{{--<div class="media mb-4">--}}
{{--<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">--}}
{{--<div class="media-body">--}}
{{--<h5 class="mt-0">Commenter Name</h5>--}}
{{--Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.--}}
{{--Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc--}}
{{--ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.--}}

{{--<div class="media mt-4">--}}
{{--<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">--}}
{{--<div class="media-body">--}}
{{--<h5 class="mt-0">Commenter Name</h5>--}}
{{--Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante--}}
{{--sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.--}}
{{--Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in--}}
{{--faucibus.--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="media mt-4">--}}
{{--<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">--}}
{{--<div class="media-body">--}}
{{--<h5 class="mt-0">Commenter Name</h5>--}}
{{--Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante--}}
{{--sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.--}}
{{--Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in--}}
{{--faucibus.--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}