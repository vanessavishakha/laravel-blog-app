@extends('layout')

@section('content')


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-8">
                <!-- Title -->
                <h1 class="mt-4">{{ $post->title }}</h1>

                <hr>

                <!-- Date/Time -->
                <p>Posted on {{ $post->created_at->format('M j, Y') }}</p>

                <hr>

                <!-- Preview Image -->
                @if($post->image != NULL)
                    <img class="img-fluid rounded" src="{{ \Storage::url($post->image) }}" alt="">
                @endif
                <hr>

                <!-- Post Content -->
                <p class="lead">{{ $post->description }}</p>

                <hr>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

@endsection
