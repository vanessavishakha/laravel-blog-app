@extends('layout')

@section('content')

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-8">

                <p class="m-0 text-center text-white">EDIT POST</p>

                <form method="POST" action="{{ route('post.edit',$post->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Title"
                               value="{{ $post->title }}">
                        @if ($errors->has('title'))
                            <span class="help-block alert alert-danger">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description"
                                  placeholder="Description">{{ $post->description }}</textarea>
                        @if ($errors->has('description'))
                            <span class="help-block alert alert-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="image">Upload an image</label>
                        <input type="file" id="blogFile" name="blogFile">
                    </div>
                    <div class="form-group">
                        @if($post->publish)
                            <label for="publish">Unpublish</label>
                            <input type="checkbox" name="publish" value="0">
                        @else
                            <label for="publish">Publish</label>
                            <input type="checkbox" name="publish" value="1">
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                    @if(count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </form>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

@endsection
