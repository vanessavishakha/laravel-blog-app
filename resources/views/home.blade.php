@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                    @endif

                    <!-- Page Content -->
                        <div class="container">

                            <div class="row">

                                <!-- Post Content Column -->
                                <div class="col-lg-8">
                                    <!-- Title -->
                                    @foreach($posts as $post)
                                        <h1 class="mt-4">{{ $post->title }}</h1>

                                        <hr>

                                        <!-- Date/Time -->
                                        <p>Posted on {{ $post->created_at }}</p>

                                        <hr>

                                        <!-- Preview Image -->
                                        @if($post->image != NULL)
                                            <img class="img-fluid rounded" src="{{ \Storage::url($post->image) }}" alt="">
                                        @endif

                                        <hr>

                                        <!-- Post Content -->
                                        <p class="lead">{{ $post->description }}</p>

                                        <hr>
                                    @endforeach
                                </div>

                            </div>
                            <!-- /.row -->

                        </div>
                        <!-- /.container -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

